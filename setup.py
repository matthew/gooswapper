#!/usr/bin/env python3

from distutils.core import setup

setup(name='gooswapper',
      version=0.3,
      description='Exchange / Google calendar sync',
      author='Matthew Vernon',
      author_email='mv3@sanger.ac.uk',
      url='https://salsa.debian.org/matthew/gooswapper',
      scripts=['gooswapper.py'],
      requires=['exchangelib ( = 1.12.2 )', 'googleapiclient ( >= 1.5.5 )'],
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Environment :: Console',
          'Intended Audience :: End Users/Desktop',
          'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
          'Operating System :: OS Independent',
          'Programming Language :: Python :: 3',
          'Topic :: Communications;',
          ],
      )
