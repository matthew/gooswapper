
** TODO known issues

- [ ] Update to use a recent exchangelib (e.g. ServiceAccount has been
  done away with in favour of FaultTolerance)
- [ ] Replace oauthclient2 use with google-auth-oauthlib (cf
  https://google-auth.readthedocs.io/en/latest/oauth2client-deprecation.html
  )
- [X] have option to re-make gcal event even if link exists when doing
  matching (e.g. so if you've previously used a test gcal, it'll make
  events in the new gcal)
- [ ] Have options for timezone handling (use exchange, use gcal, use
  a third TZ, set explicitly...)
- [ ] work out what to do with invitations (nothing?). They don't
  appear as exchangelib.items.MeetingRequest AFAICT; report upstream?
- [X] the is_recurring field is lies - use RecurringMaster type instead
- [X] command-line arguments for accounts
- [X] support multiple cache files (e.g. 1 per exchange calendar)
- [ ] note which linked account corresponds with which cache file
- [ ] configuration file?
- [ ] bi-directional syncing...
- [X] have a looping option where we sleep and then re-sync
- [ ] Make the looping interval configurable
- [ ] handle events with no gcal_id link (typically where we can't
  write one) cf commit message on 64f11e9
